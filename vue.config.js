/*
 * @Author: wang 1062637288@qq.com
 * @Date: 2023-01-12 16:18:13
 * @LastEditors: wang 1062637288@qq.com
 * @LastEditTime: 2023-03-15 13:08:46
 * @FilePath: \wang\vue.config.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
const { defineConfig } = require('@vue/cli-service')
const BASE_URL = process.env.NODE_ENV === 'production' ? '/blog/' : './'
module.exports = defineConfig({
  transpileDependencies: true,//  babel-loader默认忽略node_modules依赖包，如果需要编译node_modules中的依赖包，需要在babel.config.js中配置
  publicPath: BASE_URL,//  部署应用包时的基本 URL
  lintOnSave: false,//关闭eslint
  /*  devServer: { 
     proxy: { 
       'uploads': { 
         target: 'http://localhost:8080',
         changeOrigin: true,
         pathRewrite: {
           '^/uploads': '/uploads'
         }
       }
     }
   } */
})
