/*
 * @Author: wang 1062637288@qq.com
 * @Date: 2023-01-12 16:18:13
 * @LastEditors: wang 1062637288@qq.com
 * @LastEditTime: 2023-04-10 16:51:07
 * @FilePath: \web\src\store\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Vue from 'vue'
import Vuex from 'vuex'
import { ListApi } from '@/utils/api/ListApi'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    list: [],
    loading: false,
    isEmail: 123456,
    isPassword: 123456789
  },
  getters: {
  },
  mutations: {
    initList(state, list) {
      state.list = list

    },
    setLoading(state, loading) {
      state.loading = loading
    }

  },
  actions: {
    async getList(context) {
      const { data: res } = await ListApi()
      console.log(res)
      context.commit('initList', res)
    }
  },
  modules: {

  }
})
