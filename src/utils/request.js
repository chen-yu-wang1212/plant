import axios from 'axios'
import loading from './Loading';
import { load } from '@amap/amap-jsapi-loader';
const service = axios.create({ 
  baseURL: 'https://www.fastmock.site/mock/011450570e51ab52d68ae5c5f0c9728e/api',// api的base_url
  timeout: 5000
})
// 添加请求拦截器 前端给后端发送数据【没有到后端】
service.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  loading.startLoading()
  return config;
}, function (error) {
  // 对请求错误做些什么
  loading.endLoading()
  return Promise.reject(error);
});

// 添加响应拦截器 后端给前端返回数据【后端到前端了】
service.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  loading.endLoading()
  return response.data;
}, function (error) {
 
  // 对响应错误做点什么
  loading.endLoading()
  return Promise.reject(error);
});
export default service

