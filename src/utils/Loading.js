import store from '@/store'
let loadingNun = 0
// let loadingInstance = null
console.log(global.location.hash)
const startLoading = () => {
  if (global.location.hash !== '/login') {
    loadingNun++
    store.commit('setLoading', true)
  }
}
const endLoading = () => {
  if (global.location.hash !== '/login') {
    loadingNun--
    loadingNun = loadingNun < 0 ? 0 : loadingNun
    if (loadingNun <= 0) {
      store.commit('setLoading', false)
    }
  }
}
const resetLoading = () => {
  loadingNun = 0
  store.commit('setLoading', false)
}
export default {
  startLoading,
  endLoading,
  resetLoading
}