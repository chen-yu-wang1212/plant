/*
 * @Author: wang 1062637288@qq.com
 * @Date: 2023-04-05 18:59:06
 * @LastEditors: wang 1062637288@qq.com
 * @LastEditTime: 2023-04-07 16:32:49
 * @FilePath: \web\src\utils\api\ListApi.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import service from "../request";
export const ListApi = () => {
  return service.get('/List', {
    timeout: 5000,


  }
  )
}