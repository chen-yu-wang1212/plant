/*
 * @Author: wang 1062637288@qq.com
 * @Date: 2023-01-12 16:18:13
 * @LastEditors: wang 1062637288@qq.com
 * @LastEditTime: 2023-03-01 11:29:03
 * @FilePath: \wang\src\main.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE 
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/Js/iconfont'
import Iconsvg from './components/icon-svg.vue'

//配置安全密钥
import tagCloud from '../node_modules/TagCloud/dist//TagCloud.min'
import Video from 'video.js'
import 'video.js/dist/video-js.css'

Vue.prototype.$video = Video  // 在vue的原生里添加了Video这个标签，增强了vue的功能性

Vue.use(tagCloud)
Vue.use(ElementUI)
Vue.component('icon-svg', Iconsvg)
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

