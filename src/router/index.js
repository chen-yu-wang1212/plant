/*
 * @Author: wang 1062637288@qq.com
 * @Date: 2023-01-12 16:18:13
 * @LastEditors: wang 1062637288@qq.com
 * @LastEditTime: 2023-04-11 19:38:39
 * @FilePath: \wang\src\router\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store/index';
Vue.use(VueRouter);

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};
const routes = [
  {
    path: '/',
    name: 'Main',
    component: () => import(/* webpackChunkName: "about" */ '../views/Main.vue'),
    redirect: '/home',
    children: [
      {
        path: '/textnode',
        name: 'Textnode',
        component: () => import(/* webpackChunkName: "about" */ '../views/Textnode.vue')
      },

      {
        path: '/message',
        name: 'Message',
        component: () => import(/* webpackChunkName: "about" */ '../views/Message.vue')
      },
      {
        path: '/home',
        name: 'Home',
        meta: {
          isCheck: true
        },
        component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
      },
      {
        path: '/content',
        name: 'Content',
        component: () => import(/* webpackChunkName: "about" */ '../views/Content.vue')
      },
      {
        path: '/user',
        name: 'User',
        component: () => import(/* webpackChunkName: "about" */ '../views/User.vue')
      }
    ]
  }
];

const router = new VueRouter({
  // history: true,
  routes
});

export default router;
